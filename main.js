import './particles.js';
import $ from "jquery";
import { gsap } from "gsap";
import CanvasJS from '@canvasjs/charts';

let tl = gsap.timeline({defaults : {opacity:0, ease:'power3'}});

let STEP = 1;
let welcome = true;
let chart1, chart2, chart3, chartBar;

particlesJS.load('particles-js', 'assets/particles.json' , function() {
  console.log('callback - particles-js config loaded');
});

window.onload = () => {
  CanvasJS.addColorSet("betArabia",['#c7911d']);
  chart1 = new CanvasJS.Chart("chartContainer1", {
    backgroundColor: "transparent",
    height:300,
    theme: 'dark1',
    animationEnabled: true,
    
    title:{
      fontFamily: "Myriad",
      text:"Emails"
    },
    axisX:{
      interval: 1
    },
    axisY2:{
      interlacedColor: "rgba(199,145,29,.2)",
      gridColor: "rgba(199,145,29,.1)",
    },
    data: [{
      type: "bar",
      name: "companies",
      axisYType: "secondary",
      color: "#c7911d",
      dataPoints: [
        { y: 15, label: "Oct-23" },
        { y: 12, label: "Sep-23" },
        { y: 15, label: "Aug-23" },
        { y: 11, label: "Jul-23" },
        { y: 8, label: "Jun-23" },
        { y: 9, label: "May-23" },
        { y: 7, label: "Apr-23" },
        { y: 7, label: "Mar-23" },
        { y: 9, label: "Feb-23" },
        { y: 5, label: "Jan-23" },
        { y: 7, label: "Dec-22" },
        { y: 3, label: "Nov-22" },
      ]
    }]
  });

  chart2 = new CanvasJS.Chart("chartContainer2", {
    backgroundColor: "transparent",
    height:300,
    theme: 'dark1',
    animationEnabled: true,
    
    title:{
      fontFamily: "Myriad",
      text:"Chats"
    },
    axisX:{
      interval: 1
    },
    axisY2:{
      interlacedColor: "rgba(199,145,29,.2)",
      gridColor: "rgba(199,145,29,.1)",
    },
    data: [{
      type: "bar",
      name: "companies",
      axisYType: "secondary",
      color: "#c7911d",
      dataPoints: [
        { y: 15, label: "Oct-23" },
        { y: 12, label: "Sep-23" },
        { y: 15, label: "Aug-23" },
        { y: 11, label: "Jul-23" },
        { y: 8, label: "Jun-23" },
        { y: 9, label: "May-23" },
        { y: 7, label: "Apr-23" },
        { y: 7, label: "Mar-23" },
        { y: 9, label: "Feb-23" },
        { y: 5, label: "Jan-23" },
        { y: 7, label: "Dec-22" },
        { y: 3, label: "Nov-22" },
      ]
    }]
  });

  chart3 = new CanvasJS.Chart("chartContainer3", {
    backgroundColor: "transparent",
    height:300,
    theme: 'dark1',
    animationEnabled: true,
    
    title:{
      fontFamily: "Myriad",
      text:"Total Tickets"
    },
    axisX:{
      interval: 1
    },
    axisY2:{
      interlacedColor: "rgba(199,145,29,.2)",
      gridColor: "rgba(199,145,29,.1)",
    },
    data: [{
      type: "bar",
      name: "companies",
      axisYType: "secondary",
      color: "#c7911d",
      dataPoints: [
        { y: 15, label: "Oct-23" },
        { y: 12, label: "Sep-23" },
        { y: 15, label: "Aug-23" },
        { y: 11, label: "Jul-23" },
        { y: 8, label: "Jun-23" },
        { y: 9, label: "May-23" },
        { y: 7, label: "Apr-23" },
        { y: 7, label: "Mar-23" },
        { y: 9, label: "Feb-23" },
        { y: 5, label: "Jan-23" },
        { y: 7, label: "Dec-22" },
        { y: 3, label: "Nov-22" },
      ]
    }]
  });

  chartBar = new CanvasJS.Chart("chartContainerBar", {
    colorSet: "betArabia",
    backgroundColor: "transparent",
    height:500,
    animationEnabled: true,
    theme: "dark1",
    axisY : {
      suffix : '%'
    },
    axisX : {
      labelFontSize: 10
    },
    data: [{        
      type: "column",  
      dataPoints: [      
        { y: 25,  label: "Documents/Verification" },
        { y: 22,  label: "Deposits/Withdrawals" },
        { y: 6,  label: "Registration/Activation/Login" },
        { y: 6,  label: "Deactivation" },
        { y: 4, label: "Agent/Agency Player" },
        { y: 4,  label: "Sport Bet" },
        { y: 4,  label: "Poker" },
        { y: 3, label: "Issue" },
        { y: 3,  label: "Casino" },
        { y: 3,  label: "Bonus" },
        { y: 3,  label: "Cashback" },
        { y: 2,  label: "General Inquiry" },
        { y: 1, label: "Personal Info" },
      ]
    }]
  })


  setTimeout(() => {
    $('.welcome').fadeOut();
    $('#section-1').fadeIn();
    tl.from('#section-1 .toOpacity' , {
      duration: 1, 
      opacity: 0,
      stagger: 0.5,
    })
    .from('#section-1 .toRight' , {
      duration: 1,
      stagger: 0.5,
      x: -300,
    })
    .from('#section-1 .toUp' , {
      duration: 1,
      stagger:0.5,
      y:500
    });
    welcome = false;
  }, 800);
}

window.onkeyup = e => {
  if (welcome) return;
  if (e.key === 'n') {
    if (STEP === 12) return
    STEP++;
    slide(STEP);
  }
  if (e.key === 'p') {
    if (STEP === 1) return;
    STEP--;
    slide(STEP);
  }

  if (['0','1','2','3','4','5','6','7','8','9'].includes(e.key)) {
    STEP = parseInt(e.key)+2;
    slide(STEP);
  }

}

window.goToSection = step => {
  STEP = step;
  slide(STEP);
}

function animate(step) {

  tl.clear(true);
  if (document.querySelectorAll(`#section-${step} .toOpacity`).length > 0) {
    tl.from(`#section-${step} .toOpacity` , {
      duration: 1, 
      opacity: 0,
      stagger: 0.5,
      delay: 0.8
    });
  }

  if (document.querySelectorAll(`#section-${step} .toRight`).length > 0) {
    tl.from(`#section-${step} .toRight` , {
      duration: 1,
      stagger: 0.7,
      x: -200,
      delay: 0.8

    })
  }

  if (document.querySelectorAll(`#section-${step} .toUp`).length > 0) {
    tl.from(`#section-${step} .toUp` , {
      duration: 1,
      stagger:0.5,
      y:200,
      delay: 0
    });
  }
}

function slide(step) {
  if (step === 4) {
    chart1.render();
    chart2.render();
    chart3.render();
  }
  if (step === 5) {
    setTimeout(() => {
      chartBar.render()
    }, 50);
  }
  if (step === 1) {
    $('#footer-logo').fadeOut();
  } else {
    $('#footer-logo').fadeIn();
  };
  $('.section').fadeOut();
  $(`#section-${step}`).fadeIn();
  animate(step);
}
